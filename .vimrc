set t_Co=256 "256 color
set encoding=utf-8 "UTF-8 character encoding
set tabstop=4  "4 space tabs
set shiftwidth=4  "4 space shift
set softtabstop=4  "Tab spaces in no hard tab mode
set autoindent  "autoindent on new lines
set showmatch  "Highlight matching braces
set laststatus=2
set ruler  "Show bottom ruler
set equalalways  "Split windows equal size
set formatoptions=croqjl  "Enable comment line auto formatting and stuff
set listchars=tab:^-,trail:+,eol:$
set wildignore+=*.o,*.obj,*.class,*.swp,*.pyc "Ignore junk files
set title  "Set window title to file
set hlsearch  "Highlight on search
set ignorecase  "Search ignoring case
set smartcase  "Search using smartcase
set incsearch  "Start searching immediately
set scrolloff=5  "Never scroll off
set statusline=%f\ %l\|%v\ %{SyntasticStatuslineFlag()}\ %m%=%p%%\ (%Y%R) "Better statusbar
set wildmode=longest,list  "Better unix-like tab completion
set cursorline  "Highlight current line
set clipboard=unnamed  "Copy and paste from system clipboard
set lazyredraw  "Don't redraw while running macros (faster)
set autochdir  "Change directory to currently open file
set nocompatible  "Kill vi-compatibility
set wrap  "Visually wrap lines
set linebreak  "Only wrap on 'good' characters for wrapping
set backspace=indent,eol,start  "Better backspacing
set ttyfast  "Speed up vim
set shortmess=aI  "Make the error messages shorter
set nostartofline "Vertical movement preserves horizontal position
set cryptmethod=blowfish  "Use blowfish for encryption
set synmaxcol=1000   "Don't syntax highlight extra long lines

if version >= 703
	set formatoptions+=j  "Merge comment lines properly
	set hid "Makes undo work
	set undodir=~/.vim/undo "Persistent undo storage
	set undofile  "Store persistent undo to a file
	set undolevels=1000  "Keep undo history after a file is closed
	set undoreload=10000 "and reopened
	autocmd BufRead,BufNewFile *.sml set colorcolumn=80  "Bar at 80 col for SML
	autocmd BufRead,BufNewFile *.c set colorcolumn=80  "Bar at 80 col for C
endif

" Neobundle stuff
if has('vim_starting')
   set runtimepath+=~/.vim/bundle/neobundle.vim/
endif
call neobundle#rc(expand('~/.vim/bundle/'))
" Let NeoBundle manage NeoBundle
NeoBundleFetch 'Shougo/neobundle.vim'
NeoBundle 'Shougo/vimproc', { 'build' : { 'unix' : 'make -f make_unix.mak'}, }

" Original repos on github
NeoBundle 'scrooloose/syntastic'
NeoBundle 'nanotech/jellybeans.vim'
NeoBundle 'tomasr/molokai'
NeoBundle 'MaxSt/FlatColor'
NeoBundle 'chreekat/vim-paren-crosshairs'
NeoBundle 'urso/haskell_syntax.vim'
NeoBundle 'ervandew/supertab'
NeoBundle 'tehmatt/vim-syntax'
NeoBundle 'jcf/vim-latex'
NeoBundle 'wting/rust.vim'
NeoBundleCheck

" ----- Colorscheme stuff ----- "
let g:jellybeans_overrides = {'SignColumn': {'guifg': 'cf6a4c', 'guibg': '202020', 'ctermfg': 'Red', 'ctermbg': '233'}}
silent colorscheme jellybeans
filetype plugin indent on
syntax on

" ----- Special File Defaults ----- "
autocmd Filetype python setlocal softtabstop=4 expandtab tabstop=4 shiftwidth=4
autocmd Filetype cpp setlocal softtabstop=4 expandtab tabstop=4 shiftwidth=4
autocmd BufWritePre * :%s/\s\+$//e
autocmd BufWritePost *.tex silent call Tex_RunLaTeX()
autocmd BufWritePost *.tex silent !pkill -USR1 xdvi.bin


" ----- Plugin/Misc Settings ----- "
let g:matchparen_insert_timeout=5
let g:syntastic_check_on_open=1
let g:syntastic_error_symbol = '✗'
let g:syntastic_warning_symbol = '⚠'
let g:syntastic_auto_loc_list = 2 " Autoshow error list
let g:syntastic_c_compiler_options = '-std=c99 -Wall -Wextra'
let g:syntastic_c_include_dirs = [ 'includes', 'include', 'inc', 'headers' ]
let g:syntastic_c_auto_refresh_includes = 1
let g:syntastic_c_check_header = 1
let g:syntastic_c_remove_include_errors = 1
let g:syntastic_cpp_compiler_options = '-std=c++11'
let g:tex_flavor='latex'
let g:Tex_DefaultTargetFormat='pdf'
let g:SuperTabDefaultCompletionType = "context"


" ----- Custom Mappings ----- "
" Get rid of escape button
map <F1> <Esc>
imap <F1> <Esc>
"Session management
map <F2> :mksession! .session.vim <cr>
map <F3> :source .session.vim <cr>
" Get rid of warning on save/exit typo
command WQ wq
command Wq wq
command W w
command Q q
" :w!! does a sudo write
cnoreabbrev w!! w !sudo tee % >/dev/null
