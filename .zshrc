# Case sensitive autocompletion
CASE_SENSITIVE="true"

# Red dots while waiting for completion (too bad it doesn't work)
COMPLETION_WAITING_DOTS="true"

## Prompts
PS1='%n@%m:%~>' # prompt just in case of bash
PROMPT='%{%F{white}%}%n@%m:%~>%{%f%}%' # default prompt
RPROMPT='%{%F{white}%}[%*]%{%f%}%' # right-hand prompt

## Zsh options
setopt NOBGNICE # keep background processes at full speed
setopt HUP # restart running processes on exit
setopt PROMPT_SUBST # keep history file between sessions
setopt INC_APPEND_HISTORY # share history between zsh processes
setopt HIST_FIND_NO_DUPS # Don't find dupes of a command
setopt HIST_EXPIRE_DUPS_FIRST # Remove duplicates first if history too large
setopt SHARE_HISTORY
HISTSIZE=10001
SAVEHIST=10000
HISTFILE=~/.history
setopt APPEND_HISTORY
setopt NO_BEEP # don't beep at me
setopt AUTO_CD # running directory name does a cd
setopt CORRECT # correct mistakes
setopt AUTO_LIST
setopt COMPLETE_IN_WORD # allow tab completion in the middle of a word
setopt ALWAYS_TO_END # tab completion moves to end of word
setopt AUTO_PUSHD # change cd to pushd
setopt PUSHD_SILENT # ignore pushd messages
setopt PUSHD_TO_HOME # blank pushd goes to home
setopt SHORT_LOOPS # shorthand style for loop
setopt listtypes
setopt interactivecomments # %1 killed. will show up exactly when it is killed.
LISTMAX=0 # automatically decide when to page a list of completions
MAILCHECK=0
limit coredumpsize 0
unsetopt flowcontrol
unsetopt correctall # Turn off annoying file creation feature

##key setups
bindkey -v # vim key bindings
bindkey ' ' magic-space # history expansion on space

## Completions
autoload -U compinit
compinit -C

## case-insensitive (all),partial-word and then substring completion
zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2 eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

## Match current input in history
bindkey '^[[A' history-beginning-search-backward
bindkey '^[[B' history-beginning-search-forward

## Aliases
alias :q='exit'
alias cstyle='astyle --style=java --indent-switches --indent-cases --indent-col1-comments --indent-       labels --break-blocks --pad-oper --pad-header --delete-empty-lines --align-pointer=type --align-          reference=name --break-closing-brackets --add-brackets --max-code-length=100'

## Functions
recur-replace() {
if [[ $# -ge 3 ]]; then
    echo Too many arguments >&2
    return -1
elif [[ $# -le 1 ]]; then
    echo Not enough arguments >&2
    return -1
else
    echo Recursively replacing $1 with $2.
        grep -rl $1 * | xargs sed -i 's/'$1'/'$2'/g'
fi
}

## Exports
export EDITOR=vim

source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# OPAM configuration
. /home/mbryant/.opam/opam-init/init.zsh > /dev/null 2> /dev/null || true
